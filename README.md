Apache docker images for different versions of PHP.

**NOT FOR PRODUCTION USE**

# Tags

- latest
- 5.3 - Ubuntu 12.04 - build fails!
- 5.5 - Ubuntu 14.04 - build fails!
- 5.6 - Ubuntu 20.04 + ondrej PPA with http2
- 7.0 - Ubuntu 20.04 + ondrej PPA with http2
- 7.1 - Ubuntu 20.04 + ondrej PPA with http2
- 7.2 - Ubuntu 20.04 + ondrej PPA with http2
- 7.3 - Ubuntu 20.04 + ondrej PPA with http2
- 7.4 - Ubuntu 20.04 + ondrej PPA with http2
- 8.0 - Ubuntu 20.04 + ondrej PPA with http2
- 8.1 - Ubuntu 20.04 + ondrej PPA with http2
- 8.2 - Ubuntu 20.04 + ondrej PPA with http2

# Usage

```
docker run --rm -v "$(pwd):/var/www/html" -w /app bravomedia/apache-php:7.0
docker run --rm -v "$(pwd):/var/www/html" -v"my-apache-config:/etc/apache2/apache2.conf:ro" -w /app bravomedia/apache-php:7.0
```

# Add new PHP version

- Add version to `build-source.sh`
- Update Dockerfiles
- Test build `./build-image.sh 8.1` (try `./build-image.sh 8.1 --no-cache` if problems occur)
- Update docker hub `docker push {image}` or `./push-all` to push all images

# Notes

HTTP/2 does not work with prefork, see https://http2.pro/doc/Apache.

# TODO

- Rename default site to 000-default?
- Remove default SSL site?
