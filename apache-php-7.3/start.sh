#!/bin/bash

export PHP_VERSION=7.3

if [[ "$APACHE_USER" == "" ]]; then
  APACHE_USER="www-data"
fi

if [[ "$APACHE_USER" != "www-data" ]]; then
  adduser --disabled-password --gecos "" $APACHE_USER
  sed -i "s/.*export APACHE_RUN_USER=.*/export APACHE_RUN_USER=$APACHE_USER/g" /etc/apache2/envvars
  sed -i "s/.*export APACHE_RUN_GROUP=.*/export APACHE_RUN_GROUP=$APACHE_USER/g" /etc/apache2/envvars
fi

if [[ "$APACHE_UID" != "" ]]; then
  usermod -u $APACHE_UID $APACHE_USER
fi

if [[ "$APACHE_GID" != "" ]]; then
  groupmod -g $APACHE_GID $APACHE_USER
fi

source /etc/apache2/envvars

if [[ 0 ]]; then
  exec service varnish start &
fi

if [[ -f /etc/apache2/mods-enabled/proxy_fcgi.load ]]; then
  sed -i "s/.*clear_env =.*/clear_env = no/g" /etc/php/$PHP_VERSION/fpm/pool.d/www.conf
  if [[ "$APACHE_USER" != "www-data" ]]; then
    sed -i "s/user = www-data/user = $APACHE_USER/g" /etc/php/$PHP_VERSION/fpm/pool.d/www.conf
    sed -i "s/group = www-data/group = $APACHE_USER/g" /etc/php/$PHP_VERSION/fpm/pool.d/www.conf
  fi
  mkdir /run/php
  exec php-fpm$PHP_VERSION &
  sleep 1
  chmod 777 /run/php/*.sock # FIXME PLZ :(
fi

exec apache2 -DFOREGROUND "$@"
