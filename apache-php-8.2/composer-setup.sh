#!/bin/bash
php -r "copy('https://composer.github.io/installer.sig', 'composer-setup.sig');"
EXPECTED_SIGNATURE="$(cat "composer-setup.sig")"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
rm composer-setup.{php,sig}
chmod +x "composer.phar" || exit 1
mv "composer.phar" /usr/local/bin/composer || exit 1
