#!/bin/bash
bash ./build-source.sh || exit 1
for x in apache-php-5.6* apache-php-7* apache-php-8*; do
    docker build $@ -t "bravomedia/${x/php\-/php\:}" "./$x" || exit 1
done
