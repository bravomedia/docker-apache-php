#!/bin/bash
bash ./build-source.sh || exit 1
v=$1
shift
docker build $@ -t "bravomedia/apache-php:$v" "./apache-php-$v" || exit 1
