#!/bin/bash

for v in 5.6 7.0 7.1 7.2 7.2-fpm 7.3 7.4 8.0 8.1; do

d="apache-php-$v"
t="$(echo $v | cut -d '-' -f 2)"
v="$(echo $v | cut -d '-' -f 1)"
ubuntu_version=20.04

[[ ! -d "$d" ]] && mkdir "$d"
cp "start.sh" "$d/start.sh"
cp "composer-setup.sh" "$d/composer-setup.sh"
sed -i "s/PHP_VERSION=undefined/PHP_VERSION=$v/" "$d/start.sh"

# 1
echo "" > "$d/Dockerfile"
sed "s/{version}/$v/g; s/{ubuntu_version}/$ubuntu_version/g" << EOF >> "$d/Dockerfile"
FROM ubuntu:{ubuntu_version}

RUN apt-get update
RUN apt-get install -y --no-install-recommends software-properties-common
RUN LC_ALL=C.UTF-8 apt-add-repository -y ppa:ondrej/php
RUN LC_ALL=C.UTF-8 apt-add-repository -y ppa:ondrej/apache2
RUN apt-key update
RUN apt-get update
RUN apt-get install -y \\
    php{version} \\
    php{version}-apcu \\
    php{version}-cli \\
    php{version}-common \\
    php{version}-curl \\
    php{version}-fpm \\
    php{version}-gd \\
    php{version}-gmp \\
    php{version}-imagick \\
    php{version}-intl \\
    php{version}-json \\
    php{version}-mbstring \\
    php{version}-mcrypt \\
    php{version}-mongo \\
    php{version}-mysql \\
    php{version}-readline \\
    php{version}-redis \\
    php{version}-soap \\
    php{version}-sqlite \\
    php{version}-tidy \\
    php{version}-xdebug \\
    php{version}-xml \\
    php{version}-xmlrpc \\
    php{version}-zip \\
    apache2 \\
    libapache2-mod-php{version} \\
    unzip \\
    libimage-exiftool-perl \\
    imagemagick \\
    git \\
    jpegoptim \\
    optipng \\
    pngquant \\
    gifsicle \\
    curl

RUN update-ca-certificates
RUN ln -sf /dev/stderr /var/log/apache2/error.log
RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
RUN a2dismod mpm_event && a2enmod mpm_prefork
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2enmod http2
RUN a2enmod expires
RUN a2enmod headers
RUN rm /etc/apache2/sites-enabled/*
RUN mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/050-default.conf
RUN mv /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/050-default-ssl.conf
RUN a2ensite 050-default
RUN a2ensite 050-default-ssl
RUN update-alternatives --set php /usr/bin/php{version}
RUN apache2ctl configtest
RUN phpdismod -s cli xdebug
EOF

# php-json is preinstalled in 8.0+
if [[ "$v" = 8* ]]; then
    sed -i "/php${v}-json/d" "$d/Dockerfile"
fi

# Enable FPM?
if [[ $t = "fpm" ]]; then

sed s/{version}/$v/g << EOF >> "$d/Dockerfile"
RUN a2enmod proxy_fcgi
RUN a2enconf php7.2-fpm
RUN a2dismod php7.2
RUN a2dismod mpm_prefork
RUN a2enmod mpm_event
EOF

fi

# Stuff
sed s/{version}/$v/g << EOF >> "$d/Dockerfile"
ADD composer-setup.sh /
RUN chmod +x composer-setup.sh
RUN /composer-setup.sh

EXPOSE 80
EXPOSE 443

ADD start.sh /
RUN chmod +x start.sh

CMD ["/start.sh"]
EOF

done

# Legacy
for v in 5.3 5.3-xcache 5.5; do

    d="apache-php-$v"

    [[ ! -d "$d" ]] && mkdir "$d"
    cp "start.sh" "$d/start.sh"
    cp "composer-setup.sh" "$d/composer-setup.sh"

done

# Remove mcrypt from 7.2+
for d in ./apache-php-7*; do
    v="$(echo $d | cut -d '-' -f 3)"
    if (( $(echo "$v > 7.1" | bc -l) )); then
        sed -i "/mcrypt/d" "$d/Dockerfile"
    fi
done
