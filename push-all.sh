#!/bin/bash
bash ./build-source.sh || exit 1
for x in apache-php-5.6* apache-php-7* apache-php-8*; do
    docker push "bravomedia/${x/php\-/php\:}" || exit 1
done
